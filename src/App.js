import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import Card from './components/Card';
import { useState } from 'react';

function App() {
  const [counter, setCounter] = useState(0);

  const process = (operation) => {
    if (operation === "ADD") {
      setCounter(counter + 1);
    } else if (operation === "SUBTRACT" && counter > 0) {
      setCounter(counter - 1);
    }
  }

  return (
    <>
      <div className='container'>
        <div className='row'>
          <div className='d-flex justify-content-center align-items-center' style={{ height: '100vh' }}>
            <Card counter={counter} process={process} />
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
