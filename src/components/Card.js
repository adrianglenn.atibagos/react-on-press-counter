import React from 'react'
import Button from './Button';
import Display from './Display';

export default function Card({ counter, process }) {
    return (
        <>
            <div className='col-lg-3 col-md-3 col-sm-12'>
                <div className='card'>
                    <div className='card-body'>
                        <div className='row text-center py-5'>
                            <Display counter={counter} />
                        </div>
                        <div className='row'>
                            <div className='d-flex justify-content-center'>
                                <Button action="ADD" process={process} />
                                <Button action="SUBTRACT" process={process} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
