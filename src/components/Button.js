import React from 'react'

export default function Button({ action, process }) {
    if (action === "ADD") {
        return (
            <button type='button' className='btn btn-primary mx-1 p-4' value={action} onClick={() => process( action )}> + </button>
        )
    } else if (action === "SUBTRACT") {
        return (
            <button type='button' className='btn btn-secondary mx-1 p-4' value={action} onClick={() => process( action )}> - </button>
        )
    }
}
