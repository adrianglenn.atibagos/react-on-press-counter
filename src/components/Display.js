import React from 'react'

export default function Display({ counter }) {
    return (
        <h1>
            {counter}
        </h1>
    )
}
